import scrapy


class FsboSpider(scrapy.Spider):
    name = "zillow"
    pages = 20
    # start_urls = [
    #     'http://www.forsalebyowner.com/search/list/GA/fsbo-source/100000:-price/1-page/price,-sort',
    # ]
    # start_urls = ['http://www.zillow.com/homes/fsbo/GA/16_rid/100000-_price/375-_mp/globalrelevanceex_sort/35.45172,-79.667359,29.878755,-86.698609_rect/6_zm/%d_p/0_mmm/' %n for n in range(1, pages+1)]
    
    #100 to 140k
    pages1 = 17
    start_urls = ['http://www.zillow.com/homes/fsbo/16_rid/100000-140000_price/376-527_mp/0_singlestory/globalrelevanceex_sort/35.45172,-79.667359,29.878755,-86.698609_rect/6_zm/%d_p/0_mmm/' %n for n in range(1, pages1+1)]

    pages2 = 19
    start_urls = start_urls + ['http://www.zillow.com/homes/fsbo/16_rid/140000-190000_price/527-715_mp/0_singlestory/globalrelevanceex_sort/35.45172,-79.667359,29.878755,-86.698609_rect/6_zm/%d_p/0_mmm/' %n for n in range(1, pages2+1)]

    pages3 = 16
    start_urls = start_urls + ['http://www.zillow.com/homes/fsbo/16_rid/190000-250000_price/715-941_mp/0_singlestory/globalrelevanceex_sort/35.45172,-79.667359,29.878755,-86.698609_rect/6_zm/%d_p/0_mmm/' %n for n in range(1, pages3+1)]

    pages4 = 16
    start_urls = start_urls + ['http://www.zillow.com/homes/fsbo/16_rid/250000-350000_price/941-1318_mp/0_singlestory/globalrelevanceex_sort/35.45172,-79.667359,29.878755,-86.698609_rect/6_zm/%d_p/0_mmm/' %n for n in range(1, pages4+1)]

    pages5 = 20
    start_urls = start_urls + ['http://www.zillow.com/homes/fsbo/16_rid/350000-_price/1318-_mp/0_singlestory/globalrelevanceex_sort/35.45172,-79.667359,29.878755,-86.698609_rect/6_zm/%d_p/0_mmm/' %n for n in range(1, pages5+1)]

    #start_urls = ['http://www.forsalebyowner.com/search/list/GA/fsbo-source/100000:-price/%d-page/price,-sort' %n for n in range(1,pages+1)]

    def parse(self, response):
        # page = response.url.split("/")[-2]

        urls = response.css("a::attr(href)").re("/homedetails/.*")
        urls = ['http://www.zillow.com{0}'.format(i) for i in urls]
        for link in urls:
            yield {
            'url': link,
            }
        # urls = response.css("a::attr(href)").re("/homedetails/.*")
            
            
            



