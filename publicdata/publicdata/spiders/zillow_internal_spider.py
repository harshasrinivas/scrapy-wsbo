import scrapy
import csv
from collections import defaultdict


class FsboSpider(scrapy.Spider):
    name = "zillow_internal"

    columns = defaultdict(list) # each value in each column is appended to a list

    with open('zillow_links.csv') as f:
        reader = csv.DictReader(f) # read rows into a dictionary format
        for row in reader: # read a row as {column1: value1, column2: value2,...}
            for (k,v) in row.items(): # go over each column name and value 
                columns[k].append(v) # append the value into the appropriate list
                                     # based on column name k

    #print(columns['listing_id'])

    start_urls = columns['url']
    #['http://www.forsalebyowner.com/listing/1976-Halls-Chapel-Rd-Crandall-GA/584bebf4b0f839092a8b4567']

    def parse(self, response):
        # page = response.url.split("/")[-2]
        yield {
        'link': response.url,
        'description': response.css("div.zsg-content-component").css("div.zsg-content-item::text").extract(),
        'price': response.css(".home-summary-row").css(".main-row").css("span::text").extract_first(),
        'address': response.xpath("//span/@data-address").extract_first(),
        #response.css(".addr_city::text").extract_first(),
        'beds': response.css(".addr_bbs::text").extract()[0],
        'baths': response.css(".addr_bbs::text").extract()[1],
        'sqft': response.css(".addr_bbs::text").extract()[2],
        # 'beds-baths-sqft': response.css(".addr_bbs::text").extract(),
        'other-features': response.css(".zsg-h4 + .zsg-sm-1-1 li::text").extract(),
        'days': response.css(".zsg-h4 + .zsg-sm-1-1 li::text").re('.*on Zillow')[0],
        'phone': response.xpath("//*[text() = 'Property Owner']//following-sibling::span/text()").extract_first(),
        }