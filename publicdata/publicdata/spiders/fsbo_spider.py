import scrapy


class FsboSpider(scrapy.Spider):
    name = "fsbo"
    pages = 106
    # start_urls = [
    #     'http://www.forsalebyowner.com/search/list/GA/fsbo-source/100000:-price/1-page/price,-sort',
    # ]
    start_urls = ['http://www.forsalebyowner.com/search/list/GA/fsbo-source/100000:-price/%d-page/price,-sort' %n for n in range(1,pages+1)]

    def parse(self, response):
        # page = response.url.split("/")[-2]

        results = response.css("div.estate")
        for seller in results:
            yield {
            'url': seller.css("div.estate-bd").css("div.estate-info").css("a::attr(href)").extract_first(),
            'listing_id': seller.css("div::attr(data-source-key)").extract_first(),
            'price': seller.css("div.estate-bd").css("div.estate-info").css("div.estateSummary-price::text").extract_first(),
            'last_updated': seller.css("div.estate-bd").css("div.estate-info").css("em.highlight-text::text").extract(),
            # last_updated = [i.encode('UTF-8') for i in last_updated],
            'summary': seller.css("div.estate-bd").css("div.estate-info").css("div.estateSummary-title::text").extract_first(),
            'address': seller.css("div.estate-bd").css("div.estate-info").css("div.estateSummary-address::text").extract(),
            'features': seller.css("div.estate-bd").css("div.estate-info").css("div.estateSummary_SM-list").css("div:not(.estateSummary-list):not(.estateSummary_SM-list)::text").extract(),
            }
            
            
            



