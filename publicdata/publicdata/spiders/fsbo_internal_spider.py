import scrapy
import csv
from collections import defaultdict


class FsboSpider(scrapy.Spider):
    name = "fsbo_internal"

    columns = defaultdict(list) # each value in each column is appended to a list

    with open('fsbo.csv') as f:
        reader = csv.DictReader(f) # read rows into a dictionary format
        for row in reader: # read a row as {column1: value1, column2: value2,...}
            for (k,v) in row.items(): # go over each column name and value 
                columns[k].append(v) # append the value into the appropriate list
                                     # based on column name k

    #print(columns['listing_id'])

    start_urls = columns['url']
    #['http://www.forsalebyowner.com/listing/1976-Halls-Chapel-Rd-Crandall-GA/584bebf4b0f839092a8b4567']

    def parse(self, response):
        # page = response.url.split("/")[-2]
        yield {
        'listing_id': response.css("li + li span::text").extract()[1],
        'street': response.css("span[itemprop=streetAddress]::text").extract(),
        'locality': response.css("span[itemprop=addressLocality]::text").extract(),
        'region': response.css("span[itemprop=addressRegion]::text").extract(),
        'zip': response.css("span[itemprop=postalCode]::text").extract(),
        'phone': response.css("div[id=nav-stats]").css("strong::text").extract(),
        }

            
            
            



